from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout

def index(request):
    response = {}
    if 'failed' in request.GET:
        response['failed'] = True
    if 'needlogin' in request.GET:
        response['needlogin'] = True
    if 'loggedout' in request.GET:
        response['loggedout'] = True
    return render(request, 'index.html', response)

def login_view(request):
    user = None
    if 'username' in request.POST and 'password' in request.POST:
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        request.session['first_name'] = user.first_name
        request.session['last_name'] = user.last_name
        return redirect('/hello')
    else:
        return redirect('/?failed')

def logout_view(request):
    # print(request)
    if request.user.is_authenticated:
        request.session.flush()
        logout(request)
        return redirect('/?loggedout')
    else:
        return redirect('/?needlogin')

        
def hello(request):
    if request.user.is_authenticated:
        response = {
            'first_name': request.session['first_name'],
            'last_name': request.session['last_name']
        }
        if request.user.first_name == '' and request.user.last_name == '':
            response['first_name'] = 'No'
            response['last_name'] = 'Name'
        return render(request, 'hello.html', response)
    else:
        return redirect('/?needlogin')

